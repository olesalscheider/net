# Copyright 2021-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="Monitoring system and time series database"
HOMEPAGE="https://prometheus.io"
DOWNLOADS="
    listed-only:
        platform:amd64? ( https://github.com/${PN}/${PN}/releases/download/v${PV}/${PNV}.linux-amd64.tar.gz )
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="-* ~amd64"
MYOPTIONS="
    platform: amd64
"

DEPENDENCIES="
    build+run:
        group/prometheus
        user/prometheus
"

WORK=${WORKBASE}/${PNV}.linux-amd64

src_install() {
    dobin {prometheus,promtool}

    insinto /etc/default
    doins "${FILES}"/${PN}

    insinto /etc/${PN}
    doins ${PN}.yml
    doins -r console_libraries consoles

    install_systemd_files

    keepdir /var/{lib,log}/${PN}
    edo chown -R prometheus:prometheus "${IMAGE}"/var/{lib,log}/${PN}
}

