# Copyright 2015-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="System-level performance monitoring and performance management"
DESCRIPTION="
Performance Co-Pilot (PCP) provides a framework and services to support system-level performance
monitoring and performance management.

The PCP open source release provides a unifying abstraction for all of the interesting performance
data in a system, and allows client applications to easily retrieve and process any subset of that
data.
"
HOMEPAGE="https://${PN}.io"
DOWNLOADS="https://dl.bintray.com/${PN}/source/${PNV}.src.tar.gz"

LICENCES="CCPL-Attribution-ShareAlike-3.0 GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    avahi
    qt5
    webapi
"

# TODO:
#    build+run:
#        dev-libs/nspr  [[ note = [ support for secure sockets mode ] ]]
#        dev-libs/nss  [[ note = [ support for secure sockets mode ] ]]
#        net-libs/cyrus-sasl  [[ note = [ support for secure sockets mode ] ]]
DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/flex
        virtual/pkg-config[>=0.9.0]
    build+run:
        user/pcp
        group/pcp
        app-arch/xz
        dev-lang/perl:*
        dev-libs/libuv[>=1.16]
        sys-apps/systemd
        sys-fs/lvm2
        sys-libs/ncurses
        sys-libs/readline:=
        avahi? ( net-dns/avahi )
        qt5? (
            x11-libs/qtbase:5
            x11-libs/qtsvg:5
        )
        webapi? (
            net-libs/libmicrohttpd[>=0.9.9]
            x11-libs/cairo[>=1.2]
        )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    --sysconfdir=/etc
    --with-group=pcp
    --with-manager
    --with-rcdir=/etc/init.d
    --with-rundir=/run/pcp
    --with-sysconfigdir=/etc
    --with-systemd
    --with-threads
    --with-transparent-decompression
    --with-user=pcp
    --without-books
    --without-dstat-symlink
    --without-optimization
    --without-perl
    --without-pmdabcc
    --without-pmdajson
    --without-pmdanutcracker
    --without-pmdapodman
    --without-pmdasnmp
    --without-python
    --without-python3
    --without-qt3d
    --without-selinux
    --without-webapps
    --without-x
    # checking for SSL_ImportFD in -lssl... no
    # configure: error: cannot enable secure sockets mode - no SSL library
    --without-secure-sockets
    # unpackaged
    --without-infiniband
    --without-perfevent
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'avahi discovery'
    'qt5 qt'
    webapi
)

src_compile() {
    emake default_pcp
}

src_install() {
    emake DIST_ROOT="${IMAGE}" install_pcp

    # don't install testsuite
    edo rm -r "${IMAGE}"/var/lib/pcp/testsuite

    # move docs
    edo mv "${IMAGE}"/usr/share/doc/{pcp-doc/*,${PNVR}}

    ! option qt5 && edo rm -r "${IMAGE}"/usr/share/pcp-gui

    # remove empty directories
    edo find "${IMAGE}" -type d -empty -delete
}

