# Copyright 2012 Ivan Dives <ivan.dives@gmail.com>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Mount a WebDAV resource as a regular file system"
DESCRIPTION="
Web Distributed Authoring and Versioning (WebDAV), an extension to the HTTP-protocol,
allows authoring of resources on a remote web server. davfs2 provides the ability to access such
resources like a typical filesystem, allowing for use by standard applications with no built-in
support for WebDAV.
davfs2 is designed to fully integrate into the filesystem semantics of Unix-like systems (mount,
umount, etc.). davfs2 makes mounting by unprivileged users as easy and secure as possible.
davfs2 does extensive caching to make the file system responsive, to avoid unnecessary network
traffic and to prevent data loss, and to cope for slow or unreliable connections.
davfs2 will work with most WebDAV servers needing little or no configuration.
"
HOMEPAGE="https://savannah.nongnu.org/projects/${PN}"
DOWNLOADS="https://download.savannah.gnu.org/releases/${PN}/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( linguas: cs de )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18.1]
    build+run:
        net-misc/neon
        group/davfs2
        user/davfs2
    suggestion:
        sys-fs/fuse [[ description = [ davfs2 can use the FUSE file system API ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    ssbindir=/usr/$(exhost --target)/bin
    --enable-nls
)

src_install() {
    default

    keepdir /var/cache/davfs2
    edo chown root:davfs2 "${IMAGE}"/var/cache/davfs2
    edo chmod 0775 "${IMAGE}"/var/cache/davfs2

    edo rmdir "${IMAGE}"/etc/davfs2/certs{/private,}
}

