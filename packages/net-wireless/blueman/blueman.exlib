# Copyright 2009 Jonathan Dahan <jedahan@gmail.com>
# Copyright 2011 Ali Polatel <alip@exherbo.org>
# Copyright 2014-2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'blueman-1.10.ebuild', which is:
#   Copyright 1999-2009 Gentoo Foundation

require option-renames [ renames=[ 'policykit polkit' ] ]
require github [ user=blueman-project release=${PV/_/.} suffix=tar.xz ]
require python [ blacklist=2 multibuild=false ]
require gsettings freedesktop-desktop gtk-icon-cache
require systemd-service

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="GTK+ Bluetooth Manager, designed to be simple and intuitive for everyday bluetooth tasks"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    nautilus [[ description = [ Sendto plugin for Nautilus ] ]]
    nemo [[ description = [ Sendto plugin for Nemo ] ]]
    polkit
    pulseaudio
    thunar [[ description = [ Sendto plugin for Thunar ] ]]
    ( linguas: af am ar ast be bg bs ca cs cy da de el en_AU en_GB es et eu fa fi fr gl he hi hr hu
               id it ja kk ko lt lv mk mr ms nb nds nl oc pl pt pt_BR ro ru sk sl sq sr sv sw ta tr
               uk vi zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-python/Cython[python_abis:*(-)?]
        sys-devel/gettext[>=0.19.7]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.32]
        gnome-bindings/pygobject:3[>=3.27.2][cairo][python_abis:*(-)?]
        net-wireless/bluez[>=5.0][obex]
    run:
        dev-python/pycairo[python_abis:*(-)?]
        gnome-desktop/adwaita-icon-theme
        sys-apps/dbus[>=1.9.18]
        sys-apps/iproute2 [[ note = [ for the ip command ] ]]
        virtual/notification-daemon
        x11-libs/gdk-pixbuf:2.0[gobject-introspection]
        x11-libs/gtk+:3[>=3.10][gobject-introspection]
        x11-libs/pango[gobject-introspection]
        nautilus? ( gnome-desktop/nautilus )
        nemo? ( cinnamon-desktop/nemo )
        polkit? ( sys-auth/polkit:1 )
        pulseaudio? ( media-sound/pulseaudio )
        thunar? ( xfce-base/Thunar )
    suggestion:
        net-apps/NetworkManager[>=1.0][gobject-introspection] [[
            description = [ Can be used to manage DUN and PANU connections ]
        ]]
        net-dns/dnsmasq [[
            description = [ Can be used to provide an internet connection to a Bluetooth device ]
        ]]
        virtual/dhcp [[
            description = [ Can be used to get/provide an internet connection to/from a Bluetooth device ]
        ]]
"

# seems to run into issues with missing POTFILES
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --enable-settings-integration
    --disable-caja-sendto
    --disable-runtime-deps-check
    --disable-static
    --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
    --with-systemduserunitdir=${SYSTEMDUSERUNITDIR}
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'nautilus nautilus-sendto'
    'nemo nemo-sendto'
    polkit
    pulseaudio
    'thunar thunar-sendto'
)

blueman_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

blueman_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

